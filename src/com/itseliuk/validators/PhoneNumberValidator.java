package com.itseliuk.validators;

import java.util.regex.Pattern;

public class PhoneNumberValidator implements IValidator{

    public static final String VALID_PHONE_NUMBER_REGEX = "^[3][7][5]\\d{2}\\s\\d{7}$";

    @Override
    public boolean validate(String value) {
        boolean isMatch = Pattern.matches(VALID_PHONE_NUMBER_REGEX,value);
        return isMatch;
    }
}
