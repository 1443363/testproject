package com.itseliuk.validators;

import java.util.regex.Pattern;

public class EmailValidator implements IValidator{

    public static final String VALID_EMAIL_ADDRESS_REGEX = "^(.+)@(.+)\\.(.+)$";

    @Override
    public boolean validate(String value) {
        boolean isMatch = Pattern.matches(VALID_EMAIL_ADDRESS_REGEX,value);
        return isMatch;
    }
}
