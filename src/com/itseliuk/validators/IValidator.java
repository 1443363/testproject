package com.itseliuk.validators;

public interface IValidator {
    boolean validate(String value);
}
