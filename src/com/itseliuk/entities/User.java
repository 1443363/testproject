package com.itseliuk.entities;

import com.itseliuk.additional.Role;
import com.itseliuk.exceptions.IncorrectEmailFormatEception;
import com.itseliuk.exceptions.IncorrectPhoneOrRoleNumberException;
import com.itseliuk.validators.EmailValidator;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private static final long serialVersionUID = -5534266442745423L;
    private static final EmailValidator emailValidator = new EmailValidator();

    private String name;
    private String surname;
    private String email;
    private List<Role> roles;
    private List<String> phoneNumbers;

    public User() {
    }


    public User(String name, String surname, String email, List<Role> roles, List<String> phoneNumbers) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        setRoles(roles);
        setPhoneNumbers(phoneNumbers);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        boolean isValid = emailValidator.validate(email);
        if (isValid) {
            this.email = email;
        } else {
            try {
                throw new IncorrectEmailFormatEception("Email should adhere to " +
                        "the following format '*******@*****.***' ");
            } catch (IncorrectEmailFormatEception e) {
                e.printStackTrace();
            }
        }
    }

    public void setRoles(List<Role> roles) {
        if (roles.size() < 4) {
            this.roles = roles;
        } else {
            try {
                throw new IncorrectPhoneOrRoleNumberException("The number of roles and phone numbers for one user" +
                        " must not exceed three.");
            } catch (IncorrectPhoneOrRoleNumberException e) {
                e.printStackTrace();
            }
        }
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        if (phoneNumbers.size() < 4) {
            this.phoneNumbers = phoneNumbers;
        } else {
            try {
                throw new IncorrectPhoneOrRoleNumberException("The number of roles and phone numbers for one user" +
                        " must not exceed three.");
            } catch (IncorrectPhoneOrRoleNumberException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }
}
