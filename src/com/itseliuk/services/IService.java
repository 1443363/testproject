package com.itseliuk.services;

import java.util.List;

public interface IService<T> {

    List<T> getAll();

    T get(String value);

    void add(T object);

    void edit(String value, T object);

    void delete(String value);

    void deleteAll();
}
