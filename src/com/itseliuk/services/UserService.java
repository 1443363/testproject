package com.itseliuk.services;

import com.itseliuk.additional.Constants;
import com.itseliuk.entities.User;
import com.itseliuk.readers.UserReader;
import com.itseliuk.writers.UserWriter;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class UserService implements IService<User> {

    private static UserService userService;
    private final UserReader reader = new UserReader();
    private final UserWriter writer = new UserWriter();

    private UserService() {
    }

    public static UserService getInstance() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    @Override
    public List<User> getAll() {
        return reader.read();
    }

    @Override
    public User get(String email) {
        try {
            User user = reader.read().stream()
                    .filter(u -> u.getEmail().equals(email))
                    .findFirst().get();
            return user;
        } catch (NoSuchElementException e) {
            System.out.println("There is no user in file with such email.");
            return null;
        }
    }

    @Override
    public void add(User user) {
        List<User> users = reader.read();
        users.add(user);
        Constants.FILE.delete();
        writer.writeAll(users);
    }

    @Override
    public void edit(String email, User user) {
        List<User> users = reader.read();
        User userForEdit = users.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst().get();

        if(userForEdit != null) {
            users.remove(userForEdit);
            users.add(user);
            Constants.FILE.delete();
            writer.writeAll(users);
        } else {
            System.out.println("There is no user in file with such email.");
        }
    }

    @Override
    public void delete(String email) {
        List<User> users = reader.read().stream()
                .filter(u -> !u.getEmail().equals(email))
                .collect(Collectors.toList());

        Constants.FILE.delete();

        writer.writeAll(users);
    }

    @Override
    public void deleteAll() {
        Constants.FILE.delete();
    }
}
