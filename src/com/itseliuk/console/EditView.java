package com.itseliuk.console;

import com.itseliuk.entities.User;
import com.itseliuk.services.UserService;

import java.util.Scanner;

public class EditView {

    private static final UserService userService = UserService.getInstance();

    public static void view(User user) {
        Scanner in = new Scanner(System.in);
        System.out.println("Хотите ли вы отредактировать данные данного пользователя? ('yes'/'no')");
        String flag = in.nextLine().toLowerCase();

        switch(flag){
            case "yes" -> System.out.println("Введите наименование поля, которое хотите отредактировать");
            case "no" -> BackView.view();
            default -> view(user);
        }

        String fieldName = in.nextLine().toLowerCase();
        if (!fieldName.equals("phonenumbers")) {
            System.out.println("Введите новое значение для данного поля: ");
        }

            switch (fieldName) {
                case "name":
                    String newValue1 = in.nextLine();
                    user.setName(newValue1);
                    userService.edit(user.getEmail(), user);
                    BackView.view();
                case "surname":
                    String newValue2 = in.nextLine();
                    user.setSurname(newValue2);
                    userService.edit(user.getEmail(), user);
                    BackView.view();
                case "email":
                    String newValue3 = in.nextLine();
                    String oldEmail = user.getEmail();
                    user.setEmail(newValue3);
                    userService.edit(oldEmail, user);
                    BackView.view();
                case "phonenumbers":
                    user.setPhoneNumbers(AddView.addPhoneNumber());
                    userService.edit(user.getEmail(), user);
                    BackView.view();
                case "roles":
                    user.setRoles(AddView.addRole());
                    userService.edit(user.getEmail(), user);
                    BackView.view();
                default: view(user);
        }
    }
}
