package com.itseliuk.console;

import com.itseliuk.services.UserService;

public class AllView {

    private static UserService userService = UserService.getInstance();

    public static void view() {
        System.out.println(userService.getAll());
        BackView.view();
    }
}
