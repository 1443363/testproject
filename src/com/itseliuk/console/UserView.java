package com.itseliuk.console;

import com.itseliuk.entities.User;
import com.itseliuk.services.UserService;

import java.util.Scanner;

public class UserView {

    private static final UserService userService = UserService.getInstance();

    public static void view() {
        int errorsCount = 3;
        Scanner in = new Scanner(System.in);
        System.out.println("Пожалуйста введите email пользователя.");
        String input = in.nextLine().toLowerCase();
        User user = userService.get(input);

        while (user == null) {
            errorsCount--;
            System.out.println("Повторите попытку ввода email. Осталось " + errorsCount + " попыток.");
            String email = in.nextLine();
            user = userService.get(email);
            if (errorsCount < 1) {
                System.exit(0);
            }

        }
        System.out.println(user.toString());

        EditView.view(user);
    }


}
