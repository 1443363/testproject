package com.itseliuk.console;

import com.itseliuk.additional.Helper;
import com.itseliuk.additional.Role;
import com.itseliuk.entities.User;
import com.itseliuk.exceptions.IncorrectPhoneNumberFormatException;
import com.itseliuk.services.UserService;
import com.itseliuk.validators.PhoneNumberValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AddView {
    private static final PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator();
    private static UserService userService = UserService.getInstance();

    public static List<String> addPhoneNumber() {
        List<String> phoneNumbers = new ArrayList<>();
        boolean isValid;
        Scanner in = new Scanner(System.in);
        while (phoneNumbers.size() < 3) {
            System.out.println("Введите корректный номер телефона в формате '375** *******'");
            String phoneNumber = in.nextLine();
            isValid = phoneNumberValidator.validate(phoneNumber);
            if (isValid) {
                phoneNumbers.add(phoneNumber);
            } else {
                try {
                    throw new IncorrectPhoneNumberFormatException("Phone number should adhere to " +
                            "the following format '375** *******' ");
                } catch (IncorrectPhoneNumberFormatException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Want to add another number ? (type 'yes' or 'no')");
            String flag = in.nextLine();
            if (flag.equals("no")) {
                break;
            }
        }
        return phoneNumbers;
    }

    public static List<Role> addRole() {
        List<Role> roles = new ArrayList<>();
        boolean isValid;
        Scanner in = new Scanner(System.in);
        while (roles.size() < 3) {
            System.out.println("Введите корректную роль 'admin/visitor/trainer' ");
            String roleName = in.nextLine();
            Role role = Helper.stringToRole(roleName);
            isValid = phoneNumberValidator.validate(roleName);
            if (role != null) {
                roles.add(role);
            } else {
                System.out.println("Incorrect role format");
            }
            System.out.println("Want to add another role ? (type 'yes' or 'no')");
            String flag = in.nextLine();
            if (flag.equals("no")) {
                break;
            }
        }
        return roles;
    }

    public static void addUser() {
        User user = new User();
        Scanner in = new Scanner(System.in);
        System.out.println("Хотите ли вы добавить нового пользователя? ('yes'/'no')");
        String flag = in.nextLine().toLowerCase();

        switch(flag){
            case "no" -> BackView.view();
            case "yes" -> System.out.println("Введите имя нового пользователя");
            default -> addUser();
        }

        String userName = in.nextLine().toLowerCase();
        user.setName(userName);
        System.out.println("Введите фамилию нового пользователя");
        String userSurname = in.nextLine().toLowerCase();
        user.setSurname(userSurname);
        System.out.println("Введите email нового пользователя");
        String userEmail = in.nextLine().toLowerCase();
        user.setEmail(userEmail);
        user.setPhoneNumbers(addPhoneNumber());
        user.setRoles(addRole());
        userService.add(user);
        BackView.view();
    }
}
