package com.itseliuk.console;

import java.util.Scanner;

public class BackView {

    public static void view() {
        Scanner in = new Scanner(System.in);
        System.out.println("Для возвращение в главное меню введите 'main'");
        System.out.println("Для завершения работы с приложением введите 'exit'");
        String input = in.nextLine().toLowerCase();

        switch(input){
            case "main":
                StartView.view();
                break;
            case "exit":
                System.exit(0);
            default:
                view();
        }
    }
}
