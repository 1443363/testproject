package com.itseliuk.console;

import com.itseliuk.services.UserService;

import java.util.Scanner;

public class StartView {

    private static final UserService userService = UserService.getInstance();

    public static void view() {
        Scanner in = new Scanner(System.in);
        System.out.println("Добро пожаловть в приложение 'Пользователь'!");
        System.out.println("Для просмотра всех пользователей из файла введите 'all'.");
        System.out.println("Для просмотра конкретного юзера введите 'email'.");
        System.out.println("Для добавления нового пользователя в файле введите 'add'.");
        System.out.println("Для завершения работы с приложением введите 'exit'");
        String input = in.nextLine().toLowerCase();

        switch (input) {
            case "all" -> AllView.view();
            case "email" -> UserView.view();
            case "exit" -> System.exit(0);
            case "add" -> AddView.addUser();
            default -> view();
        }
    }
}
