package com.itseliuk.writers;

import com.itseliuk.entities.User;

import java.util.List;

public interface IWriter<T> {
    void write(T value);
    void writeAll(List<T> values);
}
