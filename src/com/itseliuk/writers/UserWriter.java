package com.itseliuk.writers;

import com.itseliuk.additional.AppendingObjectOutputStream;
import com.itseliuk.additional.Constants;
import com.itseliuk.entities.User;

import java.io.*;
import java.util.List;

public class UserWriter implements IWriter<User>{

    @Override
    public void write(User user) {
        boolean append = Constants.FILE.exists();
        ObjectOutputStream oos;

        try (FileOutputStream fOut = new FileOutputStream(Constants.FILE, true)) {
            if (append) {
                oos = new AppendingObjectOutputStream(fOut);
            } else {
                oos = new ObjectOutputStream(fOut);
            }
            oos.writeObject(user);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeAll(List<User> values) {
        boolean append = Constants.FILE.exists();
        ObjectOutputStream oos;

        try (FileOutputStream fOut = new FileOutputStream(Constants.FILE, true)) {
            if (append) {
                oos = new AppendingObjectOutputStream(fOut);
            } else {
                oos = new ObjectOutputStream(fOut);
            }

            for (User user : values) {
                oos.writeObject(user);
            }

            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
