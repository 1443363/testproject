package com.itseliuk.readers;

import com.itseliuk.additional.Constants;
import com.itseliuk.entities.User;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class UserReader  implements IReader<User>{

    @Override
    public List<User> read() {
        User user;
        ArrayList<User> users = new ArrayList<>();
        boolean isHasNext = true;

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(Constants.FILE))) {
            while(isHasNext){
                try {
                    user = (User) ois.readObject();
                    users.add(user);
                } catch (EOFException e) {
                    isHasNext = false;
              }
            }
            return users;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
