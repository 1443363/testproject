package com.itseliuk.readers;

import java.util.List;

public interface IReader<T> {
    List<T> read();
}
