package com.itseliuk.additional;

public enum Role {
    ADMIN,
    VISITOR,
    TRAINER
}
