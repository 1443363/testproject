package com.itseliuk.additional;

public class Helper {

    public static Role stringToRole(String roleName){
        Role role;
        switch (roleName){
            case "admin" -> role = Role.ADMIN;
            case "visitor" -> role = Role.VISITOR;
            case "trainer" -> role = Role.TRAINER;
            default -> {
                return null;
            }
        }
        return role;
    }
}
