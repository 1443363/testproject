package com.itseliuk.exceptions;

public class IncorrectPhoneNumberFormatException extends Exception{
    public IncorrectPhoneNumberFormatException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "IncorrectPhoneNumberFormatException{}: " + getMessage();
    }
}
