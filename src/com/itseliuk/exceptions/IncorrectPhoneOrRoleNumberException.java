package com.itseliuk.exceptions;

public class IncorrectPhoneOrRoleNumberException extends Exception{

    public IncorrectPhoneOrRoleNumberException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "IncorrectPhoneOrRoleNumberException{}: " + getMessage();
    }
}
