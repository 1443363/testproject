package com.itseliuk.exceptions;

public class IncorrectEmailFormatEception extends Exception{

    public IncorrectEmailFormatEception(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "IncorrectEmailFormatEception{}: " + getMessage();
    }

}
